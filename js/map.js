L.TopoJSON = L.GeoJSON.extend({
      addData: function(jsonData) {    
        if (jsonData.type === "Topology") {
          for (key in jsonData.objects) {
            geojson = topojson.feature(jsonData, jsonData.objects[key]);
            L.GeoJSON.prototype.addData.call(this, geojson);
          }
        }    
        else {
          L.GeoJSON.prototype.addData.call(this, jsonData);
        }
      }  
    });

var colorScale = function (val){
  if (val <= 0.01) { return '#f2f0f7' } else
  if (val <= 0.027) { return '#cbc9e2' } else
  if (val <= 0.053) { return '#9e9ac8' } else
  if (val <= 0.12) { return '#756bb1' } else
  if (val > 0.12) { return '#54278f' }
};

var styleLayer = function (layer) {
  fillCol = colorScale(layer.feature.properties.ZAPSANI_VOLICI / 313169);
  layer.setStyle({
    fillColor : fillCol,
    fillOpacity: 0.8,
    color:'white',
    weight: 1,
    opacity: 1
  });
};

var map = L.map('map').setView([49.2002211, 16.6078411], 11);

map.options.maxZoom = 13;
map.options.minZoom = 10;

L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
    attribution: 'mapová data © přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'
}).addTo(map);

var brno = new L.TopoJSON();

$.getJSON('./data/brno.json')
  .done(addTopoData);

function addTopoData(topoData){  
  brno.addData(topoData);
  brno.eachLayer(styleLayer);
  brno.addTo(map);
}

L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
}).addTo(map);

brno.on('click', function(e) {
    mc = e.layer.feature.properties.NAZ_OBEC.split('-')[1]
    podil_vol = Math.round((e.layer.feature.properties.ZAPSANI_VOLICI / 313169) * 1000) / 10
    $('#info').html('<h2 class="nazmc">Brno-' + mc + ' (' + podil_vol.toString().replace('.', ',') + ' % zapsaných voličů)</h2><br>' + texty[mc]);
});